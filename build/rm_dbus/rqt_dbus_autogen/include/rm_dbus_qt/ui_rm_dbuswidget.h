/********************************************************************************
** Form generated from reading UI file 'rm_dbuswidget.ui'
**
** Created by: Qt User Interface Compiler version 5.12.8
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_RM_DBUSWIDGET_H
#define UI_RM_DBUSWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_rm_dbusWidget
{
public:
    QPushButton *r_up;
    QPushButton *r_middle;
    QPushButton *r_down;
    QPushButton *l_up;
    QPushButton *l_middle;
    QPushButton *l_down;

    void setupUi(QWidget *rm_dbusWidget)
    {
        if (rm_dbusWidget->objectName().isEmpty())
            rm_dbusWidget->setObjectName(QString::fromUtf8("rm_dbusWidget"));
        rm_dbusWidget->resize(787, 519);
        r_up = new QPushButton(rm_dbusWidget);
        r_up->setObjectName(QString::fromUtf8("r_up"));
        r_up->setGeometry(QRect(610, 50, 89, 25));
        r_middle = new QPushButton(rm_dbusWidget);
        r_middle->setObjectName(QString::fromUtf8("r_middle"));
        r_middle->setGeometry(QRect(610, 90, 89, 25));
        r_down = new QPushButton(rm_dbusWidget);
        r_down->setObjectName(QString::fromUtf8("r_down"));
        r_down->setGeometry(QRect(610, 130, 89, 25));
        l_up = new QPushButton(rm_dbusWidget);
        l_up->setObjectName(QString::fromUtf8("l_up"));
        l_up->setGeometry(QRect(110, 50, 89, 25));
        l_middle = new QPushButton(rm_dbusWidget);
        l_middle->setObjectName(QString::fromUtf8("l_middle"));
        l_middle->setGeometry(QRect(110, 90, 89, 25));
        l_down = new QPushButton(rm_dbusWidget);
        l_down->setObjectName(QString::fromUtf8("l_down"));
        l_down->setGeometry(QRect(110, 130, 89, 25));

        retranslateUi(rm_dbusWidget);

        QMetaObject::connectSlotsByName(rm_dbusWidget);
    } // setupUi

    void retranslateUi(QWidget *rm_dbusWidget)
    {
        rm_dbusWidget->setWindowTitle(QApplication::translate("rm_dbusWidget", "rm_dbusWidget", nullptr));
        r_up->setText(QApplication::translate("rm_dbusWidget", "r_up", nullptr));
        r_middle->setText(QApplication::translate("rm_dbusWidget", "r_middle", nullptr));
        r_down->setText(QApplication::translate("rm_dbusWidget", "r_down", nullptr));
        l_up->setText(QApplication::translate("rm_dbusWidget", "l_up", nullptr));
        l_middle->setText(QApplication::translate("rm_dbusWidget", "l_middle", nullptr));
        l_down->setText(QApplication::translate("rm_dbusWidget", "l_down", nullptr));
    } // retranslateUi

};

namespace Ui {
    class rm_dbusWidget: public Ui_rm_dbusWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_RM_DBUSWIDGET_H
