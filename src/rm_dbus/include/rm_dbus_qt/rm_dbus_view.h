//
// Created by luotinkai on 2021/6/25.
//

#ifndef PROJECT_RM_DBUS_RESOURCES_RM_DBUS_VIEW_H_
#define PROJECT_RM_DBUS_RESOURCES_RM_DBUS_VIEW_H_

#include <QWidget>
#include <QPainter>
#include <QDrag>
#include <QMouseEvent>
#include <QtMath>
#include <QTimer>
#include <QDebug>
#include "ros/ros.h"
#include <rqt_gui_cpp/plugin.h>
#include "../include/rm_dbus_qt/rm_dbuswidget.h"

typedef struct {
  int16_t ch0;
  int16_t ch1;
  int16_t ch2;
  int16_t ch3;
  uint8_t s0;
  uint8_t s1;
  int16_t wheel;

  int16_t x;
  int16_t y;
  int16_t z;

  uint8_t l;
  uint8_t r;
  uint16_t key;

} DBusData_t;

namespace rm_dbus {
QT_BEGIN_NAMESPACE
namespace Ui { class rm_dbus_view; }
QT_END_NAMESPACE


class rm_dbus_view : public rqt_gui_cpp::Plugin {
 Q_OBJECT

 public:
  explicit rm_dbus_view(QWidget *parent = nullptr);
   ~rm_dbus_view() override;

 private:
  virtual void initPlugin(qt_gui_cpp::PluginContext& context);
  virtual void shutdownPlugin();
  virtual void saveSettings(qt_gui_cpp::Settings& plugin_settings, qt_gui_cpp::Settings& instance_settings) const;
  virtual void restoreSettings(const qt_gui_cpp::Settings& plugin_settings, const qt_gui_cpp::Settings& instance_settings);
  rm_dbusWidget *widget_;
  ros::Publisher dbus_pub_;
  DBusData_t d_bus_data_{};
};
} // rm_dbus

#endif //PROJECT_RM_DBUS_RESOURCES_RM_DBUS_VIEW_H_
