/********************************************************************************
** Form generated from reading UI file 'rm_dbuswidget.ui'
**
** Created by: Qt User Interface Compiler version 5.12.8
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_RM_DBUSWIDGET_H
#define UI_RM_DBUSWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QWidget>
namespace rm_dbus {
    QT_BEGIN_NAMESPACE

    class Ui_rm_dbusWidget {
    public:

        void setupUi(QWidget *rm_dbusWidget) {
            if (rm_dbusWidget->objectName().isEmpty())
                rm_dbusWidget->setObjectName(QString::fromUtf8("rm_dbusWidget"));
            rm_dbusWidget->resize(400, 300);

            retranslateUi(rm_dbusWidget);

            QMetaObject::connectSlotsByName(rm_dbusWidget);
        } // setupUi

        void retranslateUi(QWidget *rm_dbusWidget) {
            rm_dbusWidget->setWindowTitle(QApplication::translate("rm_dbusWidget", "rm_dbusWidget", nullptr));
        } // retranslateUi

    };
}
    namespace Ui {
class rm_dbusWidget : public rm_dbus::Ui_rm_dbusWidget {};
    }
    // namespace Ui
    QT_END_NAMESPACE

#endif // UI_RM_DBUSWIDGET_H
