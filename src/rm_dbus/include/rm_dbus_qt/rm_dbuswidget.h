//
// Created by luotinkai on 2021/7/1.
//

#ifndef PROJECT_RM_DBUS_SRC_RM_DBUSWIDGET_H_
#define PROJECT_RM_DBUS_SRC_RM_DBUSWIDGET_H_

#include <QWidget>
#include "rqt_gui_cpp/plugin.h"
namespace Ui { class rm_dbusWidget; }
namespace rm_dbus {

class rm_dbusWidget : public QWidget {
 Q_OBJECT

 public:
  explicit rm_dbusWidget(QWidget *parent = nullptr);
  ~rm_dbusWidget() override;

 private:
  Ui::rm_dbusWidget *ui;
 signals:
  void keyNumchanged(float num, float num2);

 private:
  int handleX;//摇杆
  int handleY;
  int handleR;

  QTimer *tim;
  float getXNum();
  float getYNum();
  int record_ax1;
  int record_ay1;
  int record_ax2;
  int record_ay2;
};
}
#endif //PROJECT_RM_DBUS_SRC_RM_DBUSWIDGET_H_
