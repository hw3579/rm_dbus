//
// Created by luotinkai on 2021/7/1.
//

// You may need to build the project (run Qt uic code generator) to get "ui_rm_dbusWidget.h" resolved

#include "../include/rm_dbus_qt/rm_dbuswidget.h"
#include "../include/rm_dbus_qt/ui_rm_dbuswidget.h"
#include "../include/rm_dbus_qt/handle.h"
#include <rqt_gui_cpp/plugin.h>
namespace rm_dbus {
rm_dbusWidget::rm_dbusWidget(QWidget *parent) :
    QWidget(parent), ui(new Ui::rm_dbusWidget) {
  ui->setupUi(this);
  setWindowTitle(QString("遥控器"));
  resize(800, 600);

  rm_dbus::Handle w;
  w.setParent(this);
  w.move(0, 350);

  rm_dbus::Handle w2;
  w2.setParent(this);
  w2.move(600, 350);

  QTimer *tim1;
  QTimer *tim2;
  tim1 = new QTimer(&w);
  tim2 = new QTimer(&w2);
  connect(tim1, &QTimer::timeout, &w, [=] {
    emit keyNumchanged(getXNum(), getYNum());
  });

  connect(tim2, &QTimer::timeout, &w2, [=] {
    emit keyNumchanged(getXNum(), getYNum());
  });

  connect(&w, &rm_dbus::Handle::keyNumchanged, &w, [=]()mutable {

    record_ax1 = getXNum();
    record_ay1 = getYNum();

    qDebug() << record_ax1 << "and" << record_ay1 << endl;
    //emit dealtomain(ax,ay);=======================================================================================
  });

  connect(&w2, &rm_dbus::Handle::keyNumchanged, &w2, [=]()mutable {

    record_ax2 = getXNum();
    record_ay2 = getYNum();

    qDebug() << record_ax2 << "and" << record_ay2 << endl;
    //emit dealtomain(ax,ay);=======================================================================================
  });


}

rm_dbusWidget::~rm_dbusWidget() {
  delete ui;
}

float rm_dbusWidget::getXNum() {
  float x;
  x = (float) (handleX - 100) / 0.75;
  return x;

}
float rm_dbusWidget::getYNum() {
  float y;
  y = -(float) (handleY - 100) / 0.75;
  return y;
}
}
#include"../include/rm_dbus_qt/rm_dbuswidget.moc"
