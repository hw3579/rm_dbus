//
// Created by luotinkai on 2021/6/25.
//

// You may need to build the project (run Qt uic code generator) to get "ui_rm_dbus_view.h" resolved

#include "../include/rm_dbus_qt/rm_dbus_view.h"
#include "../include/rm_dbus_qt/rm_dbuswidget.h"
#include "../include/rm_dbus_qt/handle.h"
#include<QWidget>
#include<QDebug>
#include <rqt_gui_cpp/plugin.h>
#include <pluginlib/class_list_macros.h>

#include "../include/rm_dbus_qt/handle.h"
#include<QWidget>
#include<QtWidgets>
#include<QApplication>
#include<QPushButton>
#include<QDebug>
#include <QWidget>
#include <QPainter>
#include <QDrag>
#include <QMouseEvent>
#include <QtMath>
#include <QTimer>
#include <QDebug>
#include<iostream>
#include<ros/ros.h>
PLUGINLIB_EXPORT_CLASS(rm_dbus::rm_dbus_view, rqt_gui_cpp::Plugin)

namespace rm_dbus {

rm_dbus_view::rm_dbus_view(QWidget *parent)
: rqt_gui_cpp::Plugin() {
  setObjectName("rm_dbus_view");

}

rm_dbus_view::~rm_dbus_view() {
  delete widget_;
}



void rm_dbus_view::initPlugin(qt_gui_cpp::PluginContext &context) {
  widget_ = new rm_dbusWidget();

}


void rm_dbus_view::shutdownPlugin() {
  // TODO unregister all publishers here

}

void rm_dbus_view::saveSettings(qt_gui_cpp::Settings &plugin_settings, qt_gui_cpp::Settings &instance_settings) const {
  // TODO save intrinsic configuration, usually using:
  // instance_settings.setValue(k, v)
}

void rm_dbus_view::restoreSettings(const qt_gui_cpp::Settings &plugin_settings,
                               const qt_gui_cpp::Settings &instance_settings) {
  // TODO restore intrinsic configuration, usually using:
  // v = instance_settings.value(k)
}

}//rqt_gui_cpp
#include"../include/rm_dbus_qt/rm_dbus_view.moc"
