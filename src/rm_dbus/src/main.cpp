#include "../include/rm_dbus_qt/handle.h"
#include<QWidget>
#include<QtWidgets>
#include<QApplication>
#include<QPushButton>
#include<QDebug>
#include <QWidget>
#include <QPainter>
#include <QDrag>
#include <QMouseEvent>
#include <QtMath>
#include <QTimer>
#include <QDebug>
#include<iostream>
#include<ros/ros.h>
namespace rm_dbus {
    int main(int argc, char **argv) {
        ros::init(argc, argv, "node1");
        ROS_INFO("hello");
        QApplication a(argc, argv);
        QWidget b;
        b.setWindowTitle(QString("遥控器"));
        b.resize(800, 600);


        rqt_gui_cpp::Handle w;
        w.setParent(&b);
        w.move(0, 350);


        rqt_gui_cpp::Handle w2;
        w2.setParent(&b);
        w2.move(600, 350);


        QPushButton up, mid, down, show;
        up.setParent(&b);
        mid.setParent(&b);
        down.setParent(&b);

        up.setText("上");
        mid.setText("中");
        down.setText("下");
        show.setText("没有模式");
        up.move(650, 50);
        mid.move(650, 100);
        down.move(650, 150);
        show.move(450, 100);

        QObject::connect(&up, &QPushButton::pressed, &up, &QPushButton::hide);
        QObject::connect(&up, &QPushButton::pressed, &mid, &QPushButton::show);
        QObject::connect(&up, &QPushButton::pressed, &down, &QPushButton::show);

        QObject::connect(&mid, &QPushButton::pressed, &up, &QPushButton::show);
        QObject::connect(&mid, &QPushButton::pressed, &mid, &QPushButton::hide);
        QObject::connect(&mid, &QPushButton::pressed, &down, &QPushButton::show);

        QObject::connect(&down, &QPushButton::pressed, &up, &QPushButton::show);
        QObject::connect(&down, &QPushButton::pressed, &mid, &QPushButton::show);
        QObject::connect(&down, &QPushButton::pressed, &down, &QPushButton::hide);


        b.show();
        return a.exec();
    }
}



