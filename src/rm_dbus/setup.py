from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup

d = generate_distutils_setup(
    packages=['rm_dbus'],
    package_dir={'': 'src'},
    scripts=['scripts/rm_dbus_plugin.py']
)

setup(**d)