import sys

from rqt_gui.main import Main


def add_arguments(parser):
    group = parser.add_argument_group('Options for rm_dbus_plugin plugin')
    group.add_argument(
        '--rmdbus-config',
        default="",
        action='store',
        help='Load an xml plot configuration')
    group.add_argument(
        '--rmdbus-run-all',
        action='store_true',
        help='Run all plots on startup')

main = Main()
sys.exit(main.main(sys.argv, standalone='dbusplugin', plugin_argument_provider=add_arguments))
